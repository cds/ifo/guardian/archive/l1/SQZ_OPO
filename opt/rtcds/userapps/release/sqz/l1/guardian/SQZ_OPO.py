# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_OPO.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_OPO.py $

from guardian import GuardState, GuardStateDecorator
from guardian.state import TimerManager
#from guardian.ligopath import userapps_path
#from subprocess import check_output
import time
import cdsutils
import gpstime
import numpy as np
#import sys

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig
from noWaitServo import Servo

from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)

# this is to keep emacs python-mode happy from lint errors due to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    log = None
    notify = None

if sqzconfig.nosqz == True:
        nominal = 'IDLE'
#    nominal = 'LOCKED_WITH_CLF'
else:
    nominal = 'LOCKED'
#nominal = 'LOCKED_and_SLOW'
#nominal = 'IDLE'

def notify_log(msg):
    notify(msg)
    log(msg)


# use a class with properties to access the RCG Epics-managed settings. If a module for setting is prefered like in the DRMI Guardian,
# then it is a drop-in replacement as both use attribute access
class OPOParams(EzcaUser):
    # counter for lock attempts
    LOCK_ATTEMPT_COUNTER = 0
    LOCK_ATTEMPT_MAX = 5
    LOCKING_GAIN = 8

    FAULT_TIMEOUT_S        = 1
    UNLOCK_TIMEOUT_S       = 1

    intervention_msg = None

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0
        self.timer['REDLIGHT_TIMEOUT'] = 0


    def in_fault(self):
        fault = False
        warning = ''
        # check the laser output
        if ezca['SQZ-LASER_IR_DC_POWERMON'] < ezca['SQZ-SHG_GRD_BLOCK_IN_TH']:
            fault = True
            warning += 'SQZ laser power low. '

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 15 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 40:
            fault = True
            warning += 'OPO TEC temperature is out of nominal range. '
            #ezca['SQZ-SHG_TEC_LOOP_SWITCH'] = 0

        if 'LOCKED' in ezca['GRD-SQZ_SHG_STATE'] and ezca['SQZ-OPO_REFL_LF_OUTPUT'] < ezca['SQZ-OPO_GRD_NOBLOCK_REFL_TH'] and ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']:
            fault=True
            warning += 'Not enough power injected into OPO. '
            
        if fault:
            notify(warning)
            
        return fault


class OPOShared(SharedState):
    @DecoShared
    def opo(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)    
        return OPOParams()


shared = OPOShared()


#################################################
# Non Fancy Functions
#################################################

def shg_locked():
    if ezca['GRD-SQZ_SHG_STATE_S'] in ['LOCKED', 'LOCKED_and_SLOW']:
        return True
    else:
        return False

def flipper_open():
    if ezca['SQZ-SHG_FIBR_FLIPPER_READBACK'] == 1:
        return True
    else:
        return False

def is_clf_res():
    return ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'] > ezca['SQZ-CLF_GRD_RF_MIN']
#################################################
# DECORATORS
#################################################        
class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.opo.in_fault():
            if shared.opo.timer['FAULT_TIMEOUT']:
                return 'FAULT'
        else:
            shared.opo.timer['FAULT_TIMEOUT'] = shared.opo.FAULT_TIMEOUT_S


class GR_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if ezca['SQZ-OPO_REFL_LF_OUTPUT'] > ezca['SQZ-OPO_GUARD_GR_REFL_TH']:
            if shared.opo.timer['UNLOCK_TIMEOUT']:
                notify('Lost lock')
                return 'LOCKING'
            return
        else:
            shared.opo.timer['UNLOCK_TIMEOUT'] = shared.opo.UNLOCK_TIMEOUT_S

class CLF_checker(GuardStateDecorator):
    def pre_exec(self):
        if not is_clf_res():
            notify('Lost CLF')
            return 'DOWN'
        else:
            shared.opo.timer['UNLOCK_TIMEOUT'] = shared.opo.UNLOCK_TIMEOUT_S

##################################################
# STATES
##################################################

class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        return True

    def run(self):
        return True


class MANAGED(GuardState):
    request = True

    def main(self):
        return True

    def run(self):
        return True



def down_state_set():
    # restore LSC and ASC down settings
    ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #'Off' #False
    ezca['SQZ-OPO_SERVO_IN2EN'] = 0 #'Off' #False
    ezca['SQZ-OPO_SERVO_COMBOOST'] = 0
    ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0 #'Off'
    ezca['SQZ-OPO_SERVO_COMEXCEN'] = 0 #'Off'
    ezca['SQZ-OPO_SERVO_SLOWEXCEN'] = 0 #'Off'

    ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','FM2','OFF','INPUT','OFF')
    ezca['SQZ-OPO_IR_LSC_SERVO_RSET'] = 2
    ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN'] = 0

    ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] = 0

    # seed servo 0
    ezca['SQZ-SEED_PHASE_DITHER_OSC_CLKGAIN'] = 0
    ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 0
    ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','FM2','OFF')

    # AJM20230928 can't lock with large offset here
    #ezca['SQZ-OPO_SERVO_COMOFS'] = -1.0


#-------------------------------------------------------------------------------
# reset everything to the good values for acquistion.
# These values are stored in the down script.
class DOWN(GuardState):
    index = 1
    request = False
    goto = True

    #@fault_checker
    def main(self):
        down_state_set()

    #@fault_checker
    def run(self):
        return True


#-------------------------------------------------------------------------------
class FAULT(GuardState):
    index = 2
    request = False
    redirect = False

    def main(self):
        down_state_set()


    def run(self):
        if not shared.opo.in_fault():
            return True
        else:
            return

#-------------------------------------------------------------------------------
class IDLE(GuardState):
    index = 3

    def run(self):
        return True

#-------------------------------------------------------------------------------
class SWEEP(GuardState):
    index = 4

    @fault_checker
    def main(self):
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_IN2EN'] = 'Off'

        self.direction = True
        return

    @fault_checker
    def run(self):
        if ezca['SQZ-OPO_GRD_SWEEP_RATE'] < 0:
            ezca['SQZ-OPO_GRD_SWEEP_RATE'] = -ezca['SQZ-OPO_GRD_SWEEP_RATE']

        if ezca['SQZ-OPO_GRD_SWEEP_LOW'] > ezca['SQZ-OPO_GRD_SWEEP_HIGH']:
            ezca['SQZ-OPO_GRD_SWEEP_LOW'], ezca['SQZ-OPO_GRD_SWEEP_HIGH'] = ezca['SQZ-OPO_GRD_SWEEP_HIGH'], ezca['SQZ-OPO_GRD_SWEEP_LOW']

        if self.direction:
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += ezca['SQZ-OPO_GRD_SWEEP_RATE'] / 16.
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > ezca['SQZ-OPO_GRD_SWEEP_HIGH']:
                self.direction = False
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = ezca['SQZ-OPO_GRD_SWEEP_HIGH']
        else:
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] -= ezca['SQZ-OPO_GRD_SWEEP_RATE'] / 16.
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] < ezca['SQZ-OPO_GRD_SWEEP_LOW']:
                self.direction = True
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = ezca['SQZ-OPO_GRD_SWEEP_LOW']
        return True


##############################################################################
# New states. 
##############################################################################
class PRESCAN_SEED(GuardState):
    index = 5
    goto = False
    

    def init_servo(self):
        #servo initialization
        #ezca['SQZ-OPO_SERVO_IN1GAIN'] = 24
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_COMBOOST'] = 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 'Off'

    def main(self):
        self.init_servo()

        self.sweepmin = 0
        self.sweepmax = 4
        self.sweepstep = 0.06

        self.maxGR = -100
        self.minGR = 100
        self.maxIR = -100
        self.minIR = 100

        self.counter = 0

        self.init_ofs = ezca['SQZ-OPO_SERVO_SLOWOUTOFS']
        ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepmin

        ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] = 0  # failure count
        
        ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1 # close BDIV
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 'Off'
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 'On' # open seed flipper
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 'Off' #switch fiber switch
        
        time.sleep(3)

        self.startgps = gpstime.gpsnow()

    def run(self):
        if self.counter == 0:
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepstep            
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepmax:
                self.endgps = gpstime.gpsnow()
                self.counter += 1

        elif self.counter == 1:
            notify('fetching REFL data')
            _data = cdsutils.getdata(['L1:SQZ-OPO_REFL_LF_OUT_DQ','L1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ'],self.endgps-self.startgps,self.startgps)
            _GRrefl = _data[0].data
            _IRrefl = _data[1].data
            self.maxGR = max(_GRrefl)
            self.minGR = min(_GRrefl)
            self.avgGR = np.mean(_GRrefl)
            
            self.maxIR = max(_IRrefl)
            self.minIR = min(_IRrefl)

            ezca['SQZ-OPO_GUARD_IR_REFL_TH'] = (self.maxIR-self.minIR)*0.5 + self.minIR
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.init_ofs
            time.sleep(1.5)

            self.counter += 1

        elif self.counter == 2:
            notify('Prescan has been done :)')
            return True

#-------------------------------------------------------------------------------
class PRESCAN_GR(GuardState):
    index = 6
    goto = False
    

    def init_servo(self):
        #servo initialization
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_COMBOOST'] = 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 'Off'

    def main(self):
        self.init_servo()

        self.sweepmin = 0
        self.sweepmax = 4
        self.sweepstep = 0.06

        self.maxGR = -100
        self.minGR = 100
        self.maxIR = -100
        self.minIR = 100

        self.counter = 0

        self.init_ofs = ezca['SQZ-OPO_SERVO_SLOWOUTOFS']
        ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepmin

        ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] = 0  # failure count
        
        ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1 # close BDIV
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 'Off' # close seed flipper
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 'On' # open shg flipper
        
        time.sleep(3)

        self.startgps = gpstime.gpsnow()

    def run(self):
        if not shg_locked():
            notify('SHG is not locked')
        
        if self.counter == 0:
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepstep            
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepmax:
                self.endgps = gpstime.gpsnow()
                self.counter += 1

        elif self.counter == 1:
            notify('fetching REFL data')
            _data = cdsutils.getdata(['L1:SQZ-OPO_REFL_LF_OUT_DQ','L1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ','SQZ-OPO_SERVO_SLOWMON'],self.endgps-self.startgps,self.startgps)
            _GRrefl = _data[0].data
            _IRrefl = _data[1].data
            SLOWout = _data[2].data
            
            self.maxGR = max(_GRrefl)
            self.minGR = min(_GRrefl)
            self.avgGR = np.mean(_GRrefl)
            
            self.maxIR = max(_IRrefl)
            self.minIR = min(_IRrefl)

            ezca['SQZ-OPO_GUARD_GR_REFL_TH'] = (self.avgGR-self.minGR)*0.5 + self.minGR # was 0.7, reduced to 0.5, 20240702 BK
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.init_ofs

            # AJM20230928 commented out, doesn't make work for lossy OPO
            #ezca['SQZ-OPO_SERVO_IN1GAIN'] = 10 - round(20*np.log10(ezca['SQZ-OPO_GUARD_GR_REFL_TH']/1)) # BK 20240815
            #ezca['SQZ-OPO_SERVO_IN1GAIN'] = 8 - round(20*np.log10(ezca['SQZ-OPO_GUARD_GR_REFL_TH']/2.3)) # BK 20240815 commented this
            ezca['SQZ-OPO_SERVO_IN1GAIN'] = 8 - round(20*np.log10(ezca['SQZ-OPO_GUARD_GR_REFL_TH']/5.74))

            time.sleep(1.5)

            self.counter += 1

        elif self.counter == 2:
            notify('Prescan has been done :)')
            return True

#-------------------------------------------------------------------------------
class PRESCAN_CLF(GuardState):
    index = 7
    goto = False
    

    def init_servo(self):
        #servo initialization
        #ezca['SQZ-OPO_SERVO_IN1GAIN'] = 24
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_COMBOOST'] = 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 'Off'

    def main(self):
        self.init_servo()

        # set CLF power
        #ezca['SQZ-CLF_ISS_DRIVEPOINT'] = 9

        self.sweepmin = 0
        self.sweepmax = 4
        self.sweepstep = 0.06

        self.maxGR = -100
        self.minGR = 100
        self.maxIR = -100
        self.minIR = 100

        self.counter = 0

        self.init_ofs = ezca['SQZ-OPO_SERVO_SLOWOUTOFS']
        ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepmin

        ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] = 0  # failure count
        
        ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1 # close BDIV
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 'Off'
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 'On' # open clf flipper
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 'On' #switch fiber switch
        
        time.sleep(3)

        self.startgps = gpstime.gpsnow()

    def run(self):
        if self.counter == 0:
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepstep            
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepmax:
                self.endgps = gpstime.gpsnow()
                self.counter += 1

        elif self.counter == 1:
            notify('fetching REFL data')
            _data = cdsutils.getdata(['L1:SQZ-OPO_REFL_LF_OUT_DQ','L1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ'],self.endgps-self.startgps,self.startgps)
            _GRrefl = _data[0].data
            _IRrefl = _data[1].data
            self.maxGR = max(_GRrefl)
            self.minGR = min(_GRrefl)
            self.avgGR = np.mean(_GRrefl)
            
            self.maxIR = max(_IRrefl)
            self.minIR = min(_IRrefl)
            self.avgIR = np.mean(_IRrefl)

            ezca['SQZ-OPO_GUARD_IR_REFL_TH'] = (self.avgIR-self.minIR)*0.5 + self.minIR
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.init_ofs
            time.sleep(1.5)

            self.counter += 1

        elif self.counter == 2:
            notify('Prescan has been done :)')
            return True

#-------------------------------------------------------------------------------
class LOCKING(GuardState):    
    index = 10
    request = False

    def init_servo(self):
        OPO_INJ = ezca['SQZ-OPO_GUARD_GR_REFL_TH']/0.7 # Injection power into the OPO. Used for the gain normalization
        
        #servo initialization
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_IN2EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_COMBOOST'] = 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 'Off'
        ezca['SQZ-OPO_SERVO_COMFILTER'] = 'On'
        ezca['SQZ-OPO_SERVO_IN1GAIN'] = shared.opo.LOCKING_GAIN
        ezca['SQZ-CLF_ISS_DRIVEPOINT'] = 5 #Begum added this because it was having trouble locking
        
    @fault_checker
    def main(self):

        # sweep parameters 
        self.sweepMax = 4
        self.sweepMin = 0
        self.sweepStep = 0.01

        self.counter = 0

        self.init_servo()
        self.timeout = 120
        self.timer['timeout'] = self.timeout
        self.timer['done'] = 0
        
        self.tolerance = 0.015 #BK stole from PMC
        self.tol_check_time = 1 #BK stole from PMC
        self.subcounter = 0 # for unkick

        if not flipper_open():
            notify("open SHG FIBER flipper")
            ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 'On'

        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 'Off'
        
        # initialize servo
        self.SPLITMON_servo = Servo(ezca, 'SQZ-OPO_SERVO_SLOWOUTOFS', readback='SQZ-OPO_SERVO_SPLITMON', gain=-0.1, ugf=3)
        self.timer['servoing'] = self.tol_check_time
        time.sleep(1)

    @fault_checker
    def run(self):
        if not shg_locked():
            notify("SHG not locked")
            return

        if not self.timer['done']:
            return 

        # check REFL PD and injection SEED power.
        if ezca['SQZ-OPO_REFL_LF_OUTPUT'] < -0.5: #ignore 
            notify('OPO REFL PD power is too small. Please check SHG, shutter, and so on.')
            self.timer['timeout'] = self.timeout
            return
        elif ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 10. and ezca['SQZ-SEED_FLIPPER_CONTROL'] == 'On':
            notify('Too large SEED power is injected. Please reduce power.')
            self.timer['timeout'] = self.timeout
            return 
            
        #scan
        if self.counter == 0:
            ezca['SQZ-OPO_SERVO_IN1EN'] = 'On'
            self.timer['done'] = 3
            
            self.counter += 1
            
        elif self.counter == 1:            
            if self.timer['timeout']:
                notify('Timed out. going prescan state.')
                return 'PRESCAN_GR'
                
            if ezca['SQZ-OPO_REFL_LF_OUTPUT'] < ezca['SQZ-OPO_GUARD_GR_REFL_TH']:
                log('On resonance, engage lock')
                # if refl signal is below the threshold, boost
                ezca['SQZ-OPO_SERVO_COMBOOST'] = 1
                
                if self.subcounter == 0:
                    time.sleep(3)
                    
                    if ezca['SQZ-OPO_REFL_LF_OUTPUT'] > ezca['SQZ-OPO_GUARD_GR_REFL_TH']:
                        log('Lost lock after COMBOOST, start again')
                        self.init_servo()
                        self.counter = 0
                    else: 
                        self.subcounter += 1
                # self.counter += 1

                elif self.subcounter ==1:
                    # minimize L1:SQZ-OPO_SERVO_SPLITMON avoid kick
                    log('servoing')
                    self.SPLITMON_servo.step()
                    time.sleep(0.1)
                    if abs(ezca['SQZ-OPO_SERVO_SPLITMON']) < self.tolerance:
                        
                        if self.timer['servoing']:
                            
                            # skip norm
                            self.subcounter += 1
                            #self.timer['lockcheck'] = 1
                            
 
                elif self.subcounter ==2:
                    # now turn on the second boost
                    ezca['SQZ-OPO_SERVO_COMBOOST'] = 2
                    time.sleep(1)
                    # ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] -= 0.1
                    self.timer['lockcheck'] = 1

                    self.counter += 1

            # if the output offset is out of sweep range, go to zero
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepMax:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepMin
                time.sleep(2)

            else:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepStep
                time.sleep(0.01)

        elif self.counter == 2:
            if ezca['SQZ-OPO_REFL_LF_OUTPUT'] > ezca['SQZ-OPO_GUARD_GR_REFL_TH']:
                log('Lost lock, start again')
                self.init_servo()
                self.counter = 0
            elif self.timer['lockcheck']:
                return True

#-------------------------------------------------------------------------------
# What does this state do?
class OFS_COMP_ENGAGING(GuardState):
    index = 15
    request = False

    def init_servo(self):
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_1'] = 0
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_2'] = 0
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_3'] = 1

        #ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','FM2','FM3','FM4','FM9','FM10','OFF','FM5','FM6','FM7','FM8','ON')
        ezca.switch('SQZ-OPO_IR_LSC_SERVO','FMALL','OFF')
        ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM5','FM6','FM7','FM8','ON')
        ezca['SQZ-OPO_IR_LSC_SERVO_RSET'] = 2
        ezca.switch('SQZ-OPO_IR_LSC_I','FM2','ON')
        ezca['SQZ-OPO_IR_LSC_I_GAIN'] = 1

        ezca['SQZ-OPO_IR_LSC_OSC_FREQ'] = 6500
        ezca['SQZ-OPO_IR_LSC_PHASEROT'] = -38
        
        ezca['SQZ-OPO_IR_LSC_OSC_TRAMP'] = 0.5
        ezca['SQZ-OPO_SERVO_IN2GAIN'] = -32
        ezca['SQZ-OPO_SERVO_IN2EN'] = 'On'
        
        ezca['SQZ-OPO_IR_LSC_SERVO_TRAMP'] = 0.3
        ezca['SQZ-OPO_IR_LSC_SERVO_GAIN'] = 1
        ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN'] = .03



    @GR_lock_checker
    def main(self):
        self.init_servo()
        self.counter = 0
        self.timer['done'] = 1
        self.tol = 0.001

    @GR_lock_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            ezca.switch('SQZ-OPO_IR_LSC_SERVO','INPUT','ON')
            self.timer['done'] = 0.2
            self.timer['check_err'] = 2.2
            self.counter += 1

        elif self.counter == 1:
            if abs(ezca['SQZ-OPO_IR_LSC_SERVO_INMON']) > self.tol:
                self.timer['check_err'] = 2
                
            else:
                if self.timer['check_err']:
                    ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM5','OFF')
                    self.counter += 1

        elif self.counter == 2:
            return True
                    
#-------------------------------------------------------------------------------   
class GR_LOCKED(GuardState):
    index = 20
    
    @GR_lock_checker    
    @fault_checker
    def main(self):

        self.counter = 0 # lockloss counter
        
    @GR_lock_checker        
    @fault_checker
    def run(self):
        #check OPO locking
        if ezca['SQZ-OPO_REFL_LF_OUTPUT'] > ezca['SQZ-OPO_GUARD_GR_REFL_TH']:
            self.counter += 1
            if self.counter > 5:
                return 'LOCKING'
                
        else:
            self.counter = 0
            
        return True

#-------------------------------------------------------------------------------
class CHECKING_IR_RESONANCE(GuardState):
    index = 25
    request = False
    
    @GR_lock_checker
    def main(self):
        self.counter = 0
        self.LLcount = 0

    @GR_lock_checker        
    def run(self):

        #check that OPO is still locked on green
        if ezca['SQZ-OPO_REFL_LF_OUTPUT'] > ezca['SQZ-OPO_GUARD_GR_REFL_TH']:
            self.LLcount += 1
            if self.LLcount > 5:
                return 'LOCKING'
        else:
            self.LLcount = 0

        #check shutter and flippers
        if not ezca['SQZ-CLF_FLIPPER_CONTROL']:
            ezca['SQZ-CLF_FLIPPER_CONTROL'] = 1
            log('CLF flipper was closed. Opening it to see a CLF beat note')

        if ezca['SQZ-SEED_FLIPPER_CONTROL']:
            ezca['SQZ-SEED_FLIPPER_CONTROL'] = 0
            log('Seed flipper was open. Closing it.')

        if not ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_READBACK']:
            ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 1
            log('Fiber switch was on seed path, switching to CLF')
            return

        if ezca['SYS-MOTION_C_SHUTTER_I_TRIGGER_VOLTS'] > ezca['SYS-MOTION_C_SHUTTER_I_THRESHOLD']:
            notify_log('Too much power is on the CLF PD. Can\'t open the fast shutter')
            self.counter = 0
            return

        
 
        #average SEED(CLF) REFL
        if self.counter == 0:
            # open CLF shutter
            ezca['SYS-MOTION_C_SHUTTER_I_OPEN'] = 1
            time.sleep(0.1)
            self.counter += 1

        elif self.counter == 1:
            # checking CLF beatnote
            if is_clf_res():
                ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] = 0
                return True
            else:
                ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] += 1
                if ezca['SQZ-OPO_GUARD_LOCK_FAIL_CT'] >= 20:
                    notify_log('Too many failure to grab co-resonance. Please check CLF RF beatnote level')
                    return
                    
                notify('This is the wrong resonance. Going back to the LOCKING state.')

                # shift output offset by 1fsr
                FSR = 0.75 # Green FSR in slow offset of servo.
                
                if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > 4.0:
                    ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = 0
                else:
                    ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] +=  FSR * 0.7

                ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
                ezca['SQZ-OPO_SERVO_COMBOOST'] = 0
                ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 'Off'

                time.sleep(2)
                return 'LOCKING'

#-------------------------------------------------------------------------------
class LOCKED_WITH_CLF(GuardState):
    index = 50

    @GR_lock_checker
    @CLF_checker
    @fault_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
class LOCKED_WITH_SEED(GuardState):
    index = 55

    @GR_lock_checker    
    @fault_checker
    def main(self):
        # seed servo 0
        ezca['SQZ-SEED_PHASE_DITHER_OSC_CLKGAIN'] = 0
        ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 0
        ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','FM2','OFF')

        #check shutter and flippers
        if ezca['SQZ-CLF_FLIPPER_CONTROL']:
            ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
            
        if not ezca['SQZ-SEED_FLIPPER_CONTROL']:
            ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1

        if ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_READBACK']:
            ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0
            notify_log('Fiber switch is on Seed path.')
            return

        self.counter = 0
        
    @GR_lock_checker                
    @fault_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
class DITHER_LOCKING(GuardState):
    index = 105
    request = False

    def init_servo(self):
        #servo initialization
        # LM: these were removed and only IR_REFL is used        
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_1'] = 1
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_2'] = 0
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_3'] = 0

        ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','FM2','FM3','FM4','FM5','FM6','FM7','OFF','FM8','FM9','FM10','INPUT','ON')
        ezca.switch('SQZ-OPO_IR_LSC_I','FM2','OFF')

        ezca['SQZ-OPO_IR_LSC_OSC_FREQ'] = 3000
        ezca['SQZ-OPO_IR_LSC_PHASEROT'] = 100
        
        ezca['SQZ-OPO_IR_LSC_OSC_TRAMP'] = 0.5
        ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN'] = .1
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'        
        ezca['SQZ-OPO_SERVO_IN2EN'] = 'On'
        ezca['SQZ-OPO_SERVO_IN2GAIN'] = 0
        ezca['SQZ-OPO_IR_LSC_SERVO_TRAMP'] = 0.3
        ezca['SQZ-OPO_IR_LSC_SERVO_GAIN'] = 1

        time.sleep(1)


    def main(self):
        self.reflthrL = ezca['SQZ-OPO_GUARD_IR_REFL_TH'] # low threshold. If the REFL signal gets below this value, servo will be on.
        self.reflthrH = ezca['SQZ-OPO_GUARD_IR_REFL_TH'] # high threshold. After turning servo on, if the refl signal gets above this value, try the cavity lock again

        # sweep parameters 
        self.sweepMax = 4
        self.sweepMin = 0
        self.sweepStep = 0.03

        self.count = 0

        self.init_servo()
        self.timeout = 60
        self.timer['timeout'] = self.timeout
        # power normlization
        ezca['SQZ-OPO_IR_LSC_I_GAIN'] = 14.4/ezca['SQZ-OPO_IR_LSC_PD_IN_INMON'] * 0.1/ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN']
        
    def run(self):
        # check REFL PD and injection SEED power.
        if ezca['SQZ-OPO_REFL_LF_OUTPUT'] < -5000: #ignore 
            notify('OPO REFL PD power is too small. Please check SHG, shutter, and so on.')
            self.timer['timeout'] = self.timeout
            return
        elif ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 10. and ezca['SQZ-SEED_FLIPPER_CONTROL'] == 'On':
            notify('Too large SEED power is injected. Please power down it.')
            self.timer['timeout'] = self.timeout
            return 

            
        #scan
        if self.count == 0:
            if not ezca['SYS-MOTION_C_BDIV_E_CLOSEDSWITCH']:
                notify('Close beam diverter!!')
                return

            else:
                ezca['SQZ-SEED_FLIPPER_CONTROL'] = 'On'
                ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 'Off'
                ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 'Off'
                self.count += 1
            
            
        if self.count == 1:
            if self.timer['timeout']:
                notify('Timed out. going prescan state.')
                return 'PRESCAN_SEED'
                
            if ezca['SQZ-SHUTTER_I_TRIGGER_OUTPUT'] < self.reflthrL:
                # if refl signal is below the threshold, turn on servo.
                ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','ON')
                time.sleep(0.5)
                ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM2','ON')
                time.sleep(0.5)
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] -= 0.3
                self.timer['lockcheck'] = 1
                self.count += 1

            # if the output offset is out of sweep range, bring back to 0
            elif ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepMax:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepMin
                time.sleep(1)

                    
            else:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepStep

        elif self.count == 2:
            if ezca['SQZ-SHUTTER_I_TRIGGER_OUTPUT'] > self.reflthrH:
                self.init_servo()
                self.count = 0
            elif self.timer['lockcheck']:                
                return True

#------------------------------------
class DITHER_LOCKED(GuardState):
    index = 110

    def main(self):
        self.reflthrH = ezca['SQZ-OPO_GUARD_IR_REFL_TH']# high threshold. After turning servo on, if the refl signal gets above this value, going back to LOCKING state.
        log('threshold is %f'%self.reflthrH)
        self.counter = 0

    def run(self):
        #check OPO locking
        if ezca['SQZ-SHUTTER_I_TRIGGER_OUTPUT'] > self.reflthrH:
            self.counter += 1
            if self.counter > 5:
                return 'DITHER_LOCKING'
        else:
            self.counter = 0
            
        return True
          
class SEED_PHASE_LOCKING(GuardState):
    index = 310
    request = False

    @fault_checker
    @GR_lock_checker                    
    def main(self):
        # prepare phase locking servo
        ezca['SQZ-SEED_PHASE_DITHER_OSC_CLKGAIN'] = 0.1
        ezca['SQZ-SEED_PHASE_SEN_MTRX_1_1'] = 0        
        ezca['SQZ-SEED_PHASE_SEN_MTRX_1_2'] = -1
        ezca['SQZ-SEED_PZT_OFFSET'] = 50

        self.timer['waiting'] = 2
        self.counter = 0
        # engage servo

    @fault_checker
    @GR_lock_checker                    
    def run(self):
        if not self.timer['waiting']:
            return

        if abs(ezca['SQZ-SEED_PATH_PZT_OUTPUT']) > 25000: #initialize servo if output is on the rail
            ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 0
            ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','FM2','OFF')
            self.counter = 0
            self.timer['waiting'] = 5
            return

        if self.counter == 0:
            ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 1
            self.timer['waiting'] = 2
            self.counter += 1

        elif self.counter == 1:
            ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','ON')
            self.timer['waiting'] = 1
            self.counter += 1
            
        elif self.counter == 2:
            ezca.switch('SQZ-SEED_PHASE_SERVO','FM2','ON')
            self.timer['waiting'] = 1
            self.counter += 1

        elif self.counter == 3:
            return True

#-------------------------------------------------------------------------------
class CLF_DITHER_LOCKING(GuardState):
    index = 115
    request = False

    def init_servo(self):
        #servo initialization
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_1'] = 0
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_2'] = 1
        ezca['SQZ-OPO_IR_LSC_INMTRX_1_3'] = 0

        ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','FM2','FM4','FM5','FM6','FM7','OFF','FM3','FM8','FM9','FM10','INPUT','ON')
        ezca.switch('SQZ-OPO_IR_LSC_I','FM2','OFF')

        ezca['SQZ-OPO_IR_LSC_OSC_FREQ'] = 6500
        ezca['SQZ-OPO_IR_LSC_PHASEROT'] = -155
        
        ezca['SQZ-OPO_IR_LSC_OSC_TRAMP'] = 0.5
        ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN'] = .1
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'        
        ezca['SQZ-OPO_SERVO_IN2EN'] = 'On'
        ezca['SQZ-OPO_SERVO_IN2GAIN'] = 0
        ezca['SQZ-OPO_IR_LSC_SERVO_TRAMP'] = 0.3
        ezca['SQZ-OPO_IR_LSC_SERVO_GAIN'] = 1

        time.sleep(1)


    def main(self):
        self.reflthrL = ezca['SQZ-OPO_GUARD_IR_REFL_TH'] # low threshold. If the REFL signal gets below this value, servo will be on.
        self.reflthrH = ezca['SQZ-OPO_GUARD_IR_REFL_TH'] # high threshold. After turning servo on, if the refl signal gets above this value, try the cavity lock again

        # sweep parameters 
        self.sweepMax = 4
        self.sweepMin = 0
        self.sweepStep = 0.03

        self.count = 0

        self.init_servo()
        self.timeout = 60
        self.timer['timeout'] = self.timeout

        self.timer['done'] = 1
        
    def run(self):
        # check REFL PD and injection SEED power.
        if ezca['SQZ-OPO_REFL_LF_OUTPUT'] < -5000: #ignore 
            notify('OPO REFL PD power is too small. Please check SHG, shutter, and so on.')
            self.timer['timeout'] = self.timeout
            return
        elif ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 10. and ezca['SQZ-SEED_FLIPPER_CONTROL'] == 'On':
            notify('Too large SEED power is injected. Please power down it.')
            self.timer['timeout'] = self.timeout
            return 

        if not self.timer['done']:
            return
            

        if self.count == 0:
            ezca['SQZ-CLF_FLIPPER_CONTROL'] = 'On'
            ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 'On'
            ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 'Off'
            self.count += 1
            self.timer['done'] = 1
            self.timer['avg'] = 1
            self.N = 0
            self.val = 0
            

        elif self.count == 1:
            # power normlization
            self.N += 1
            self.val += ezca['SQZ-OPO_IR_LSC_PD_IN_INMON']
            if self.timer['avg']:
                avg = self.val/self.N
                ezca['SQZ-OPO_IR_LSC_I_GAIN'] = 70/avg * 0.1/ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN']
                self.count+=1
        #scan
        elif self.count == 2:
            if self.timer['timeout']:
                notify('Timed out. going prescan state.')
                return 'PRESCAN_CLF'
                
            if ezca['SQZ-SHUTTER_I_TRIGGER_OUTPUT'] < self.reflthrL:
                # if refl signal is below the threshold, turn on servo.
                ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','ON')
                time.sleep(0.5)
                ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM2','ON')
                time.sleep(0.5)
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] -= 0.3
                self.timer['lockcheck'] = 1
                self.count += 1

            # if the output offset is out of sweep range, bring back to 0
            elif ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepMax:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepMin
                time.sleep(1)

                    
            else:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepStep

        elif self.count == 3:
            if ezca['SQZ-SHUTTER_I_TRIGGER_OUTPUT'] > self.reflthrH:
                self.init_servo()
                self.count = 0
            elif self.timer['lockcheck']:                
                return True

#-------------------------------------------------------------------------------
class CLF_DITHER_LOCKED(GuardState):
    index = 120

    def main(self):
        self.reflthrH = ezca['SQZ-OPO_GUARD_IR_REFL_TH']# high threshold. After turning servo on, if the refl signal gets above this value, going back to LOCKING state.
        log('threshold is %f'%self.reflthrH)
        self.counter = 0

    def run(self):
        #check OPO locking
        if ezca['SQZ-SHUTTER_I_TRIGGER_OUTPUT'] > self.reflthrH:
            self.counter += 1
            if self.counter > 5:
                return 'DITHER_LOCKING'
        else:
            self.counter = 0
            
        return True

#-------------------------------------------------------------------------------          
class SEED_PHASE_LOCKING(GuardState):
    index = 310
    request = False

    @fault_checker
    @GR_lock_checker                    
    def main(self):
        # prepare phase locking servo
        ezca['SQZ-SEED_PHASE_DITHER_OSC_CLKGAIN'] = 0.1
        ezca['SQZ-SEED_PHASE_SEN_MTRX_1_1'] = 0        
        ezca['SQZ-SEED_PHASE_SEN_MTRX_1_2'] = -1
        ezca['SQZ-SEED_PZT_OFFSET'] = 50

        self.timer['waiting'] = 2
        self.counter = 0
        # engage servo

    @fault_checker
    @GR_lock_checker                    
    def run(self):
        if not self.timer['waiting']:
            return

        if abs(ezca['SQZ-SEED_PATH_PZT_OUTPUT']) > 25000: #initialize servo if output is on the rail
            ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 0
            ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','FM2','OFF')
            self.counter = 0
            self.timer['waiting'] = 5
            return

        if self.counter == 0:
            ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 1
            self.timer['waiting'] = 2
            self.counter += 1

        elif self.counter == 1:
            ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','ON')
            self.timer['waiting'] = 1
            self.counter += 1
            
        elif self.counter == 2:
            ezca.switch('SQZ-SEED_PHASE_SERVO','FM2','ON')
            self.timer['waiting'] = 1
            self.counter += 1

        elif self.counter == 3:
            return True

#-------------------------------------------------------------------------------        
class SEED_PHASE_LOCKED_MIN(GuardState):
    index = 320
    
    @fault_checker
    @GR_lock_checker                    
    def main(self):
        self.lockloss_timeout = 3
        ezca['SQZ-SEED_PHASE_SEN_MTRX_1_2'] = -1
        self.timer['lockloss'] = self.lockloss_timeout
        self.counter = 0

    @fault_checker
    @GR_lock_checker                    
    def run(self):
        if abs(ezca['SQZ-SEED_PATH_PZT_OUTPUT']) > 25000: #initialize servo if output is on the rail
            if self.timer['lockloss']:
                ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 0
                ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','FM2','OFF')
                return 'SEED_PHASE_LOCKING'

        else:
            self.timer['lockloss'] = self.lockloss_timeout
            return True

#-------------------------------------------------------------------------------
class SEED_PHASE_LOCKED_MAX(GuardState):
    index = 330
    
    @fault_checker
    @GR_lock_checker                    
    def main(self):
        self.lockloss_timeout = 3
        ezca['SQZ-SEED_PHASE_SEN_MTRX_1_2'] = 1
        self.timer['lockloss'] = self.lockloss_timeout
        self.counter = 0

    @fault_checker
    @GR_lock_checker                    
    def run(self):
        if abs(ezca['SQZ-SEED_PATH_PZT_OUTPUT']) > 25000: #initialize servo if output is on the rail
            if self.timer['lockloss']:
                ezca['SQZ-SEED_PHASE_SERVO_GAIN'] = 0
                ezca.switch('SQZ-SEED_PHASE_SERVO','FM1','FM2','OFF')
                return 'SEED_PHASE_LOCKING'

        else:
            self.timer['lockloss'] = self.lockloss_timeout
            return True
            
#-------------------------------------------------------------------------------
class OFS_COMP_DISABLE(GuardState):
    index = 400

    @GR_lock_checker
    @CLF_checker
    @fault_checker
    def main(self):
        self.counter = 2 #0 #AJM20230928 skip servoing
        self.timer['done'] = 0
        self.tolerance = 100

    @GR_lock_checker
    @CLF_checker
    @fault_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            # create servo
            self.timer['in_Tol'] = 2
            self.servo = Servo(ezca, 'SQZ-OPO_SERVO_COMOFS',
                                         readback='SQZ-OPO_IR_LSC_SERVO_OUTPUT',
                                         gain=-0.001, ugf=0.3, setpoint=0)

            self.counter += 1
            
        elif self.counter == 1:
            if abs(ezca['SQZ-OPO_IR_LSC_SERVO_OUTPUT']) < self.tolerance:
                if self.timer['in_Tol']:
                    self.counter += 1
            else:
                self.timer['in_Tol'] = 2
                
            self.servo.step()
            
        if self.counter == 2:
            ezca.switch('SQZ-OPO_IR_LSC_SERVO','INPUT','OFF')
            ezca['SQZ-OPO_SERVO_IN2EN'] = 'Off'
            ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN'] = 0
            self.counter += 1

        elif self.counter == 3:
            return True

        
#-------------------------------------------------------------------------------
class LOCKED(GuardState):
    index = 500
    
    @GR_lock_checker
    @CLF_checker
    @fault_checker
    def run(self):
        return True
                

##################################################

edges = [
    ('INIT'     , 'DOWN')    ,
    ('DOWN'     , 'IDLE')    ,
    ('IDLE'     , 'SWEEP')   ,

    ('IDLE','LOCKING'),
    ('LOCKING','GR_LOCKED'),
    ('GR_LOCKED','CHECKING_IR_RESONANCE'),
    ('CHECKING_IR_RESONANCE','OFS_COMP_ENGAGING'),
    ('OFS_COMP_ENGAGING','LOCKED_WITH_CLF'),
    ('CHECKING_IR_RESONANCE','LOCKED_WITH_CLF'),
    ('LOCKED_WITH_CLF','OFS_COMP_DISABLE'),
    ('OFS_COMP_DISABLE','LOCKED'),
    
    
    ('IDLE','PRESCAN_SEED'),    
    ('PRESCAN_SEED','IDLE'),
    ('PRESCAN_GR','IDLE'),
    ('IDLE','PRESCAN_GR'),
    
    ('IDLE','DITHER_LOCKING'),
    ('DITHER_LOCKING','DITHER_LOCKED'),

    ('IDLE','PRESCAN_CLF'),    
    ('PRESCAN_CLF','CLF_DITHER_LOCKING'),
    ('CLF_DITHER_LOCKING','CLF_DITHER_LOCKED'),
    
    ('LOCKED_WITH_CLF','LOCKED_WITH_SEED'),
    ('LOCKED_WITH_SEED','SEED_PHASE_LOCKING'),    
    ('SEED_PHASE_LOCKING','SEED_PHASE_LOCKED_MIN'),
    ('SEED_PHASE_LOCKED_MIN','SEED_PHASE_LOCKED_MAX'),
    ('SEED_PHASE_LOCKED_MAX','SEED_PHASE_LOCKED_MIN'),
    ('SEED_PHASE_LOCKED_MAX','LOCKED_WITH_SEED'),
    ('SEED_PHASE_LOCKED_MIN','LOCKED_WITH_SEED'),

    ('LOCKED_WITH_SEED','CHECKING_IR_RESONANCE'),

]

#list of states which can fail out too many times and freeze in LOCKLOSS_TOOMANY
lock_states = [
    'LOCKED_WITH_CLF',
]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
